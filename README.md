# Sample project that uses GitLab Maven repository feature.


1. Automatically craete a new build with version and jar  pacakging 
2. Test the maven .
4. in deploy execute the jar.

## Using Maven CLI

To be able to donwload and upload packages from this repo using your local machine 
you need to add `gitlab-com` server section to your `~/.m2/settings.xml`

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
  <servers>
     <server>
       <id>gitlab-com</id>
       <configuration>
         <httpHeaders>
           <property>
             <name>Private-Token</name>
             <value>XXXXXXXXXX</value>
           </property>
         </httpHeaders>
       </configuration>
     </server>
   </servers>
 </settings>
```
